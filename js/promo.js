$(document).ready(function(){
	$('.countdown').countdown('2019/12/01 00:00', function (event) {
	    var $this = $(this).html(event.strftime(''
	        + '<div class="cd-elem"><div class="cd">%D</div><div class="cd-text">дней</div></div>'
	        + '<div class="cd-elem"><div class="cd">%H</div><div class="cd-text">часов</div></div>'
	        + '<div class="cd-elem"><div class="cd">%M</div><div class="cd-text">минут</div></div>'));
	});
});