$(document).ready(function () {
    $('.anchor[href^="#"]').click(function () {
        if($(window).innerWidth() <= 1000) {
            $('.header__nav').removeClass('active');
            $('.header__row.purple').removeClass('z0');
            $('.header').removeClass('z10');
        }
        elementClick = $(this).attr("href");
        if($(window).innerWidth() <= 1000) {
            destination = $(elementClick).offset().top;
        } else {
            destination = $(elementClick).offset().top-20;
        }
        $('html, body').animate( { scrollTop: destination }, 500, 'swing' );
        return false;
    });

    function mobileMenu(){
        var burger = $('.burger');
        var close = $('.close-menu');

        burger.click(function(e){
            $('.header__nav').addClass('active');
            $('.header__row.purple').addClass('z0');
            $('.header').addClass('z10');
        });
        close.click(function (e) {
            $('.header__nav').removeClass('active');
            $('.header__row.purple').removeClass('z0');
            $('.header').removeClass('z10');
        });

        if($(window).width() < 1000){
            $('.lc-mobile').append($('.header-lc'))
        }
    }

    mobileMenu();

    function mapControl() {

        if ($('.section-map').length){
            var preview = $('.map-preview');
            var explore = $('.map-explore');
    
            var mapSectY = $('.section-map').offset().top + 100;
            
            $(window).scroll(function(e){
                var scrollOffset = $(window).scrollTop();
                // console.log(e);
                if (scrollOffset >= mapSectY) {
                    activateMap();
                } else {
                    deactivateMap();
                }
            })
            function activateMap (e) {
                $('.map-sections-wrap').addClass('explore');
                // preview.removeClass('active');
                $('.js-hide').addClass('active')
                explore.addClass('active');
                setTimeout(function () {
                    $('.map-bg-left').addClass('active')
                    $('.map-bg-right').addClass('active')
                }, 1000);
                $('.map-bg').addClass('active');
                $('.map').addClass('active');
            }

            function deactivateMap(e) {
                $('.map-sections-wrap').removeClass('explore');
                // preview.removeClass('active');
                $('.js-hide').removeClass('active')
                explore.removeClass('active');
                setTimeout(function () {
                    $('.map-bg-left').removeClass('active')
                    $('.map-bg-right').removeClass('active')
                }, 1000);
                $('.map-bg').removeClass('active');
                $('.map').removeClass('active');
            }
            // var btn = $('.map-btn');
            // btn.click(activateMap)
        }

    }

    mapControl();

    function OpenPopup(popupId) {
        $('body').removeClass('no-scrolling');
        $('.popup').removeClass('js-popup-show');
        popupId = '#' + popupId;
        $(popupId).addClass('js-popup-show');
        $('body').addClass('no-scrolling');
    }

    $('.pop-op').click(function (e) {
        e.preventDefault();
        let data = $(this).data('popup');
        OpenPopup(data);
    });

    function closePopup() {
        $('.js-close-popup').on('click', function (e) {
            e.preventDefault();
            $('.popup').removeClass('js-popup-show');
            $('body').removeClass('no-scrolling');
        });
    }
    closePopup();

    function months() {
        if ($('.months').length) {
            $('.months span').click(function (e) {
                $('.months span').removeClass('active');
                $(this).addClass('active');
            })
        }
    };

    months();

    function weeks() {
        if ($('.weeks').length) {
            $('.weeks__nums span').click(function (e) {
                $('.weeks__nums span').removeClass('active');
                $(this).addClass('active');
            })
        }
    };

    weeks();

    function tabs() {
        $(".tab_item").not(":first").hide();
        $(".up__tabs-wrapper .tab").click(function () {
            if ($('.up__tabs-wrapper .tab').eq(0).hasClass('active')) {
                $('.up-bg').addClass('active');
            } else {
                $('.up-bg').removeClass('active');
            }
            $(".up__tabs-wrapper .tab").removeClass("active").eq($(this).index()).addClass("active");
            $(".tab_item").hide().eq($(this).index()).fadeIn()
        }).eq(0).addClass("active");
    }

    tabs();


    function dnd(){
        $(".dropify").dropify({
            messages: {
                'default': 'Перетащите фото в область загрузки',
                'replace': 'Перетащите фото что-бы заменить+',
                'remove': 'Удалить',
                'error': 'Произошла ошибка'
            },
            tpl: {
                wrap: '<div class="dropify-wrapper"></div>',
                loader: '<div class="dropify-loader"></div>',

                message: '<div class="dropify-message"><div class="dropzone__inner"><img src="img/dropzone-img.png" alt=""><span>{{ default }}</span></div></div>',
                preview: '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">{{ replace }}</p></div></div></div>',
                filename: '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                clearButton: '<button type="button" class="dropify-clear">{{ remove }}</button>',
                errorLine: '<p class="dropify-error">{{ error }}</p>',
                errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
            }
        });
    }

    if ($('.dropify').length) {
        dnd();
    }

    function select() {
        $('.select2').select2({
            minimumResultsForSearch: Infinity
        });
    }


    if ($('.select2').length) {
        select();
    }

    function swiper() {

        function swipperInit() {
            var wrap = $('.pd_slider');

            new Swiper(wrap, {
                slidesPerView: 3,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev'
                },

                effect: 'coverflow',
                centeredSlides: true,
                coverflowEffect: {
                    rotate: 0,
                    stretch: -100,
                    depth: 1000,
                    modifier: 1,
                    slideShadows: false
                },

                loop: true,
                speed: 400,
                observer: true,
                observeParents: true,

                preloadImages: false,
                lazy: {
                    loadPrevNext: true,
                    loadPrevNextAmount: 3
                },
                pagination: {
                    el: '.swiper-pagination',
                    type: 'bullets',
                },

                breakpoints: {
                    1000: {
                        slidesPerView: 2,
                        centeredSlides: true,
                        coverflowEffect: {
                            rotate: 0,
                            stretch: 0,
                            depth: 200,
                            modifier: 1,
                            slideShadows: false
                        },
                        

                        spaceBetween: 30
                    }
                }
            });
        }

        function swipperInitEdge() {
            var wrap = $('.pd_slider');

            new Swiper(wrap, {
                slidesPerView: 'auto',
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev'
                },

                centeredSlides: true,

                loop: true,
                speed: 400,
                observer: true,
                observeParents: true,

                preloadImages: false,
                lazy: {
                    loadPrevNext: true,
                    loadPrevNextAmount: 3
                },

                breakpoints: {
                    1000: {
                        slidesPerView: 8,
                        centeredSlides: false,

                        spaceBetween: 30
                    }
                }
            });
        }
        if (/Edge/.test(navigator.userAgent)) {
            swipperInitEdge();
        } else {
            swipperInit();
        }
    }

    if ($('.slider').length) {
        swiper();
    }

    var swiper = new Swiper('.pd_slider_detail', {
        pagination: {
            el: '.swiper-pagination',
        },
    });

    function prizesPage(){
        $('.prizesp__case').click(function(e){
            $(this).addClass('active');
        })
    }
    prizesPage();

    if ($('.popup-before-start').length){
        var startDate = $('.cd-start-date').text()
        $('.cd').countdown(startDate, function (event) {
            var $this = $(this).html(event.strftime(''
                + '<span>%D</span> д. '
                + '<span>%H</span> ч. '
                + '<span>%M</span> мин.'));
        });
    }
    function phoneInit() {
        $(".phone-mask").inputmask({
            mask:"+7(999)999-99-99",
            "clearIncomplete": true
        });
    }
    phoneInit();
    function checkValidate() {
        var form = $('form');

        $.each(form, function () {
            $(this).validate({
                ignore: [],
                errorClass: 'error',
                validClass: 'success',
                rules: {
                    name: {
                        required: true 
                    },
                    email: {
                        required: true,
                        email: true 
                    },
                    phone: {
                        required: true,
                        phone: true 
                    },
                    message: {
                        required: true 
                    },
                    password: {
                        required: true,
                        normalizer: function normalizer(value) {
                            return $.trim(value);
                        }
                    }
                },
                errorElement : 'span',
                errorPlacement: function(error, element) {
                    var placement = $(element).data('error');
                    if (placement) {
                        $(placement).append(error);
                    } else {
                        error.insertBefore(element);
                    }
                },
                messages: {
                    phone: 'Некорректный номер',
                    email: 'Некорректный e-mail'
                } 
            });
        });
        jQuery.validator.addMethod('email', function (value, element) {
            return this.optional(element) || /\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}/.test(value);
        });
        jQuery.validator.addMethod('phone', function (value, element) {
            return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
        });
    }
    checkValidate();
    function refExpPhone(form) {
        let str = $('#'+form+' .phone-mask').val();
        let re1 = new RegExp(/[-()/\\]/g);
        str = str.replace(re1,'');
        $('#'+form+' .phone-mask').inputmask('remove');
        $('#'+form+' .phone-mask').val(str);
        console.log($('#'+form+' .phone-mask').val());
    }
    // autocomplete russian cities
    var options = {
        url: "/static/js/russia.json",
        getValue: "city",
        list: { 
            match: {
                enabled: true
            }
        },
        theme: "square"
    };
    $(".autocompl").easyAutocomplete(options);
    var GLOBAL_CITY=[];
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var myObj = JSON.parse(this.responseText);
        GLOBAL_CITY = myObj.map((e) => {return e.city;});
      }
    };
    xmlhttp.open("GET", "/static/js/russia.json", true);
    xmlhttp.send();
    $(".autocompl").on('keyup change', function() {
        var city_autocomplete = $(this).val();
        var city_correct = $.inArray(city_autocomplete, GLOBAL_CITY);
        if(city_correct >= 0) {
            $(this).removeClass('error-2');
        } else if(city_correct == -1) {
            $(this).addClass('error-2');
        }
    });

    //ajax reg form
    // проверить пригласительный код ?????
    $('#reg .js-submit').click(function(e){
        e.preventDefault();
        $('#reg form').valid();
        if($('#reg form').valid() && !$('.autocompl').hasClass('error-2')) {
            refExpPhone('reg');
            var form_data = $('#reg form [name="csrfmiddlewaretoken"], #reg form [name="task"], #reg form .phone-mask, #reg form .g-recaptcha-response').serialize();
            var invite_data = $('#reg form [name="csrfmiddlewaretoken"], #reg form [name="invite_code"]').serialize();
            if($('#reg form [name="invite_code"]').val().length != 0) {
                $.ajax({
                    url:      '/check_invite',
                    type:     "POST",
                    dataType: "html",
                    data: invite_data,
                    success: function(response) {
                        response = $.parseJSON(response);
                        if(response.ok=='ok') {
                            $.ajax({
                                url:      '/check_phone',
                                type:     "POST",
                                dataType: "html",
                                data: form_data,
                                success: function(response) {
                                    response = $.parseJSON(response);
                                    if(response.ok=='ok') {
                                        OpenPopup('reg-complete');
                                        $('#reg .captcha').appendTo('#reg-complete .captcha-wrap');
                                        phoneInit();
                                    } else if(response.error == 'invalid user') {
                                        console.log('Пользователь существует или нет');
                                        phoneInit();
                                    } else if(response.error == 'invalid phone') {
                                        console.log('Некорректный номер телефона');
                                        phoneInit();
                                    } else {
                                        console.log('Чет забыл');
                                        phoneInit();
                                    }
                                },
                                error: function(response) {
                                    console.log('Ошибка. Данные не отправлены.');
                                    phoneInit();
                                }
                            });
                        } else {
                            console.log('Чет забыл');
                        }
                    },
                    error: function(response) {
                        console.log('Ошибка. Данные не отправлены.');
                    }
                });
            } else {
                $.ajax({
                    url:      '/check_phone',
                    type:     "POST",
                    dataType: "html",
                    data: form_data,
                    success: function(response) {
                        response = $.parseJSON(response);
                        if(response.ok=='ok') {
                            OpenPopup('reg-complete');
                            $('#reg .captcha').appendTo('#reg-complete .captcha-wrap');
                            phoneInit();
                            $('#reg-complete .js-close-popup').click(function(){
                                $('#reg-complete .captcha').appendTo('#reg .captcha-wrap');
                            });
                        } else if(response.error == 'invalid user') {
                            console.log('Пользователь существует или нет');
                            phoneInit();
                        } else if(response.error == 'invalid phone') {
                            console.log('Некорректный номер телефона');
                            phoneInit();
                        } else {
                            console.log('Чет забыл');
                            phoneInit();
                        }
                    },
                    error: function(response) {
                        console.log('Ошибка. Данные не отправлены.');
                        phoneInit();
                    }
                });
            }
        }   
    });
    $('#reg-complete .js-submit').click(function(e){
        e.preventDefault();
        $('#reg-complete form').valid();
        var pwd1 = $('#reg-complete [name="password"]').val();
        var pwd2 = $('#reg-complete [name="password-confirm"]').val();
        if(pwd1 != pwd2) {
            $('.password-ver').addClass('error-pwd');
            $('#reg-complete .err').text('Пароли не совпадают');
        }
        if($('#reg-complete form').valid() && (pwd1 == pwd2)) {
            $('.password-ver').removeClass('error-pwd');
            $('#reg-complete .err').text('');
            refExpPhone('reg');
            var form_data = $('#reg [name="csrfmiddlewaretoken"], #reg [name="task"], #reg [name="lastName"], #reg [name="firstName"], #reg [name="patronymic"], #reg [name="phone"], #reg [name="email"], #reg [name="city"], #reg [name="invite_code"], #reg-complete [name="code"], #reg-complete [name="password"], #reg-complete [name="g-recaptcha-response"]').serialize();
            $.ajax({
                url:      '/register',
                type:     "POST",
                dataType: "html",
                data: form_data,
                success: function(response) {
                    response = $.parseJSON(response);
                    if(response.ok=='ok') {
                        document.location.href="/lk";
                    } else if(response.error == 'user exists') {
                        console.log('Пользователь существует или нет');
                        phoneInit();
                    } else {
                        console.log('Чет забыл или неверные данные');
                        phoneInit();
                    }
                },
                error: function(response) {
                    console.log('Ошибка. Данные не отправлены.');
                    phoneInit();
                }
            }); 
        }   
    });

    //ajax auth form
    $('#auth .js-submit').click(function(e){
        e.preventDefault();
        $('#auth form').valid();
        if($('#auth form').valid()) {
            refExpPhone('auth');
            $.ajax({
                url:      '/login',
                type:     "POST",
                dataType: "html",
                data: $("#auth form").serialize(),
                success: function(response) { 
                    response = $.parseJSON(response);
                    if(response.ok=='ok') {
                        phoneInit();
                        document.location.href="/lk";
                    } else {
                        console.log('Ошибка какая-то');
                        phoneInit();
                    }
                },
                error: function(response) {
                    console.log('Ошибка. Данные не отправлены.');
                    phoneInit();
                }
            });
        }
    });

    //ajax forgot form
    $('#forgot-password .js-submit').click(function(e){
        e.preventDefault();
        $(this).addClass('disabled');
        refExpPhone('forgot-password');
        let dt = new Date();
        let time = dt.getFullYear() + '/' + (dt.getMonth()+1) + '/' + dt.getDate() + ' ' + dt.getHours() + ":" + (dt.getMinutes()+1) + ":" + dt.getSeconds();
        $('.clock').parent().show();
        $('.clock').countdown(time)
        .on('update.countdown', function(event) {
            $(this).html(event.strftime('%M:%S'));
        })
        .on('finish.countdown', function(event) {
            $(this).parent().hide();
            $(this).parents('#forgot-password').find('.js-submit').removeClass('disabled');
        });
        $.ajax({
            url:      '/',
            type:     "POST",
            dataType: "html",
            data: $("#forgot-password form").serialize(),
            success: function(response) { 
                response = $.parseJSON(response);
                if(response.ok=='ok') {
                    $(this).removeClass('disabled');
                    $('#new-password').modal('open');
                    $('#new-password .default-button.modal-close').click(function(){
                        $('#forgot-password').modal('close');
                    });
                    phoneInit();
                } else {
                    $('.clock').parent().show();
                    $('.clock').countdown(time)
                    .on('update.countdown', function(event) {
                        $(this).html(event.strftime('%M:%S'));
                    })
                    .on('finish.countdown', function(event) {
                        $(this).parent().hide();
                        $(this).parents('#forgot-password').find('.js-submit').removeClass('disabled');
                    });
                    phoneInit();
                }
            },
            error: function(response) {
                $(this).removeClass('disabled');
                phoneInit();
            }
        });
    });

    //ajax feedback form
    $('.feedback-form .js-submit').click(function(e){
        $(this).addClass('disabled');
        $.ajax({
            url:      '/feedback',
            type:     "POST",
            dataType: "html",
            data: $(".feedback-form").serialize(),
            success: function(response) { 
                response = $.parseJSON(response);
                if(response.ok=='ok') {
                    $('#feedback-success').modal('open');
                    $('.feedback-form')[0].reset();
                    $('.page-feedback .browser-default').select2('destroy');
                    $('.page-feedback .browser-default').select2({
                        placeholder: "Выбери тему из списка",
                        allowClear: true,
                        minimumResultsForSearch: Infinity
                    });
                    $('select').on('select2:open', function () {
                        $('.select2-results__options').scrollbar();
                    });
                    $('.feedback-form .js-submit').removeClass('disabled');
                } else {
                    $('#feedback-error').modal('open');
                    $('.feedback-form .js-submit').removeClass('disabled');
                }
            },
            error: function(response) {
                console.log('Ошибка. Данные не отправлены.');
                $('.feedback-form .js-submit').removeClass('disabled');
            }
        });
    });

    // choise shop
    $('#id_stores').on('select2:select', function(){
        $('#check-shop .js-submit').removeClass('disabled');
    });

    // append get shop param
    var iframe_src = $('#check-upload iframe').attr('src');
    $('#shops .js-submit').click(function(){
        if($('#shops .select2-selection__rendered').attr('title') !='') {
            var span_shop = $('#shops .select2-selection__rendered').attr('title');
            var shop_val = $('#id_stores option:contains('+span_shop+')').val();
            var new_string = String(iframe_src+'&param[shop]='+shop_val);
            OpenPopup('check-upload');
            $('#check-upload iframe').attr('src',new_string);
        }
    });

    // unsubscribe success modal
    if(window.location.href.indexOf('?unsub=true') > 0){
        OpenPopup('unsubscribe');
    }

    // set cookie
    if($.cookie("modalCookie") == null) {
        $('#cookies').addClass('js-popup-show');
    }
    else {
        $('#cookies').removeClass('js-popup-show');
    }
    $('#cookies .js-close-popup-cookie').click(function(e){
        e.preventDefault();
        $.cookie("modalCookie", 1);
        $('.popup-cookies').removeClass('js-popup-show');
        $('body').removeClass('no-scrolling');
    });
})